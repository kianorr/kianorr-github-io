---
title: "kian's website"
draft: false
cover:
  image: "<https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png>"
---

![pond in sweden](/sweden.jpg)
_A very pretty lake in Sweden_

## about me

Hello, my name is kian johan nils orr and this is my page! :) 
It features different areas of my life, 
like physics research, books, and graphic design.

___