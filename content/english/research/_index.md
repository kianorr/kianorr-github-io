---
title: "research"
draft: false
cover:
  image: "<https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png>"
  relative: false
weight: 1
---

These are all of the research projects I've done in my physics career, ranging from acoustics to plasma.
___