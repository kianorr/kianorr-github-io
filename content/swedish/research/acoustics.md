---
title: "Optimal Concert Hall"
mathjax: true
katex: true
markup: 'mmark'
math: true
draft: false
---

This simulation attempts to recreate the distribution of sound in square concert halls. 

![asfasdfa](/acoustics_BSH.png#center)

$$\phi = 5$$
$x = 5$
